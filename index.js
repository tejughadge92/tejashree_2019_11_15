let result = null;
let canvasInstances = [];
let randomCount;
let canvas0;
let canvas1;
let fabricCanvasObj;
let currentIdStr;

// This method adds random canvas from 2 to 5
function addCanvas() {
    randomCount = Math.floor(Math.random() * (5 - 2 + 1)) + 2;
    for (let i = 0; i < randomCount; i++) {
        let content = document.getElementById('content');
        let newDiv = document.createElement('div');
        newDiv.id = 'div' + i;
        newDiv.className = 'canvas-div';
        let newcanvas = document.createElement('canvas');
        newcanvas.width = '300';
        newcanvas.height = '400';
        newcanvas.style = 'border: 1px solid #000000';
        newcanvas.id = i;
        newDiv.appendChild(newcanvas);
        content.appendChild(newDiv);
        fabricCanvasObj = new fabric.Canvas(newcanvas);
        canvasInstances.push(fabricCanvasObj);
    }
}

// This method inserts into canvas image related to selected item
function insertIntoCanvas() {
    let images = document.getElementById('select-values');
    let selectedImageIndex = images.selectedIndex;
    let canvas = document.getElementById(Math.floor(Math.random() * (randomCount - 0 + 1)) + 0);
    var insertIntoCanvas = canvasInstances[canvas.id];
    var currentId = currentIdStr.slice(3);
    canvas0 = canvasInstances[0];
    canvas1 = canvasInstances[1];
    // canvas0 = canvasInstances[currentId];
    // canvas1 = canvasInstances[currentId];
    canvas1 = canvasInstances[1];
    canvas0.on("object:moving", onObjectMoving);
    canvas1.on("object:moving", onObjectMoving);

    let base_image = new Image();
    base_image.src = result[selectedImageIndex].thumbnailUrl;
    base_image.onload = function () {
        var image = new fabric.Image(base_image, {
            left: 50,
            top: 70,
        });
        insertIntoCanvas.add(image);
    }
}

//This method gets result from api and appends into select dropdown 10 values per second
function getDropDownData() {
    const url = "https://jsonplaceholder.typicode.com/photos?results=10";
    fetch(url)
        .then(
            response => response.json()
        ).then(
            html => {
                result = html;
                let images = document.getElementById('select-values');
                let startIndex = 0;
                let endIndex = 9;
                // below code gets value per second from the response title and creates option in select element
                setInterval(() => {
                    for (let i = startIndex; i < endIndex; i++) {
                        images.options[i] = new Option(html[i].title, 'selectValue' + i);
                    }
                    startIndex = endIndex;
                    endIndex = endIndex + 10;
                }, 1000)
            }
        );

    $('#content').mouseover(function () {
        $('.canvas-div').mouseover(function () {
            currentIdStr = this.id;
        });
    });
}

var onObjectMoving = function (p) {
    var viewport = p.target.canvas.calcViewportBoundaries();

    if (p.target.canvas.lowerCanvasEl.id === canvas0.lowerCanvasEl.id) {
        if (p.target.left > viewport.br.x) {
            migrateItem(canvas0, canvas1, p.target);
            return;
        }
    }
    if (p.target.canvas.lowerCanvasEl.id === canvas1.lowerCanvasEl.id) {
        if (p.target.left < viewport.tl.x) {
            migrateItem(canvas1, canvas0, p.target);
            return;
        }
    }
};

var migrateItem = function (fromCanvas, toCanvas, pendingImage) {
    // Just drop image from old canvas
    fromCanvas.remove(pendingImage);

    // We're going to trick fabric.js,
    // so we keep internal transforms of the source canvas, 
    // in order to inject it into destination canvas.
    var pendingTransform = fromCanvas._currentTransform;
    fromCanvas._currentTransform = null;

    // Make shortcuts for fabric.util.removeListener and fabric.util.addListener
    var removeListener = fabric.util.removeListener;
    var addListener = fabric.util.addListener;

    // Re-arrange subscriptions for source canvas
    {
        removeListener(fabric.document, 'mouseup', fromCanvas._onMouseUp);
        removeListener(fabric.document, 'touchend', fromCanvas._onMouseUp);

        removeListener(fabric.document, 'mousemove', fromCanvas._onMouseMove);
        removeListener(fabric.document, 'touchmove', fromCanvas._onMouseMove);

        addListener(fromCanvas.upperCanvasEl, 'mousemove', fromCanvas._onMouseMove);
        addListener(fromCanvas.upperCanvasEl, 'touchmove', fromCanvas._onMouseMove, {
            passive: false
        });
    }

    // Re-arrange subscriptions for destination canvas
    {
        addListener(fabric.document, 'touchend', toCanvas._onMouseUp, {
            passive: false
        });
        addListener(fabric.document, 'touchmove', toCanvas._onMouseMove, {
            passive: false
        });

        removeListener(toCanvas.upperCanvasEl, 'mousemove', toCanvas._onMouseMove);
        removeListener(toCanvas.upperCanvasEl, 'touchmove', toCanvas._onMouseMove);

        addListener(fabric.document, 'mouseup', toCanvas._onMouseUp);
        addListener(fabric.document, 'mousemove', toCanvas._onMouseMove);
    }

    // We need this timer, because we want Fabric.js to complete pending render
    // before we inject, because it causes some unpleasant image jumping.
    setTimeout(function () {
        // Add image to destination canvas,
        pendingImage.canvas = toCanvas;
        pendingImage.migrated = true;
        toCanvas.add(pendingImage);

        // and inject transforms from source canvas
        toCanvas._currentTransform = pendingTransform;

        // finally don't forget to make pasted object selected
        toCanvas.setActiveObject(pendingImage);
    }, 10);
};
